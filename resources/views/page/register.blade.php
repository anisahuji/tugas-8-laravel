@extends('layout.master')
@section('title')
Halaman Form
@endsection
@section('content')
    <h1>Buat Account Baru</h1>
    <h3>Sign Up Form</h3>
   <form action="/welcome" method="POST">
       @csrf
        <label>First Name :</label><br>
        <input type="text" name="first_name"><br><br>
        <label>Last Name :</label><br>
        <input type="text" name="last_name"><br><br>
        <label>Gender</label><br><br>
        <input type="radio" name="gender">Male<br>
        <input type="radio" name="gender">Female<br>
        <input type="radio" name="gender">Other<br><br>
        <label>Nationality</label><br><br>
        <select name="nationality" id="">
            <option value="1">Indonesia</option>
            <option value="2">America</option>
            <option value="3">Inggris</option>
        </select><br><br>
        <label>Language Spoken</label><br><br>
        <input type="checkbox" name="language">Bahasa Indonesia<br>
        <input type="checkbox" name="language">English<br>
        <input type="checkbox" name="language">Other<br><br>
        <label>Bio :</label><br><br>
        <textarea name="bio" cols="30" rows="10"></textarea><br>

        <input type="submit" value="sign up">
    </form>
@endsection