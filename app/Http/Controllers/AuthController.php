<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function reg()
    {
        return view('page.register');
    }
    
    public function welcome(Request $request)
    {
        //dd($request->all());
        $first = $request['first_name'];
        $last = $request['last_name'];
        return view('page.welcome', compact("first", "last"));
    }
}
