<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index');
Route::get('/register', 'AuthController@reg');
Route::post('/welcome', 'AuthController@welcome');

Route::get('/data-tables', 'IndexController@table');

//CRUD Cast
//create
Route::get('/cast/create', 'CastController@create');
Route::post('/cast', 'CastController@store');

//read
Route::get('/cast', 'CastController@index');
Route::get('/cast/{cast_id}', 'CastController@show');

//update
Route::get('/cast/{cast_id}/edit', 'CastController@edit');
Route::put('/cast/{cast_id}', 'CastController@update');

//delete
Route::delete('/cast/{cast_id}', 'CastController@destroy');